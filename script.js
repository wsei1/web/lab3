function saveInLocalStorage(e) {
	e.preventDefault();

	var full_name = document.getElementById('full_name').value;
	var email = document.getElementById('email').value;
	var fav_color = document.getElementById('fav_color').value;

	localStorage.setItem('ls_full_name', full_name);
	localStorage.setItem('ls_email', email);
	localStorage.setItem('ls_fav_color', fav_color);
}
